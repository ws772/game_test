var player = require('./player');

module.exports.game = function(){ 

    let currentPlayer = new player();

    function report(){
        console.log('Got gold: '+gold);
    }

    function updateState(isMonster){
        if(isMonster==='MONSTER'){
            currentPlayer.live--;
        }else{
            currentPlayer.gold++;
        }
        
        if(live<=0){
            console.log('Game over');
            report();
            reset();
        }
    }

    function reset(){
        console.log('Game restart');
        currentPlayer = new player();
    }

    function action(posx, posy){

        verifyPosition(posx, posy);
        const url = 'http://localhost:3000/rooms/'+posx+'/'+posy;
        
        return new Promise(function(resolve, reject) {
            axios.get(url)
              .then(response => {
                resolve(response.data);
              })
              .catch(error => {
                reject(error);
              });
            });
    }

    function execute(direction){
        currentPlayer.move(direction);
        action();
        updateState();
    }
    
    function verifyPosition(posx, posy){
        return posx>=0 && posx>=0 && posy<=24 &&posy<=24;
    }
    
}