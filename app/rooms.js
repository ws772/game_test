

module.exports.getInfo = function(req,res){
            
  function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  
  function hasMonster() {
    return getRandomInt(2)===1? 'GOLD':'MONSTER';
  }

  var posx = req.params.x, posy = req.params.y;
  res.send(hasMonster());
};